## Getting started

Click on the Binder button to launch the notebook in a temporary interactive environment: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mariakzurek%2Fhigh-twist/HEAD?filepath=parametrized.ipynb)

